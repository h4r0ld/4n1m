#include "animation.h"
#include "preview.h"
#include "commands.h"
#include "editor.h"
#include "timeline.h"
#include "titlebar.h"
#include "toolbar.h"
#include "selecttool.h"

Editor::Editor(MainWindow* mw): QWidget(mw)
{
    mainwindow = mw;
    setCursor(Qt::CrossCursor);
    selectTool = new SelectTool(mainwindow);
    setGeometry(0, 25, mainwindow->getWindowDimensions().width(), mainwindow->getWindowDimensions().width() - 300);
    setMouseTracking(true);
    layerImage = QImage(width(), height(), QImage::Format_ARGB32);

    int imgX = width()/2 - animation()->animSize.width()/2;
    int imgY = height()/2 - animation()->animSize.height()/2 - 25;
    int imgH =animation()->animSize.height();
    int imgW = animation()->animSize.width();

    backgroundImage = QImage(width(), height(), QImage::Format_ARGB32);
    backgroundImage.fill(editorBackgroundColor);
    QPainter p(&backgroundImage);
    p.setCompositionMode(QPainter::CompositionMode_DestinationOut);
    p.setPen(Qt::transparent);
    p.setBrush(Qt::black);
    p.drawRect(imgX, imgY, imgW, imgH);
    p.setCompositionMode(QPainter::CompositionMode_SourceOver);
    p.setPen(QColor(0, 0, 0, 1));
    p.setBrush(QColor(0, 0, 0, 1));
    p.drawRect(imgX -5, imgY - 5, imgW + 10, imgH + 10);
}

void Editor::mousePressEvent(QMouseEvent* event)
{
    mainwindow->subtoolbar->hideProperties();
    if (
        !animation()->isKey(timeline()->getLayer(), timeline()->getPos()) &&
        currentTool != EMPTY &&
        currentTool != ERASER &&
        currentTool != SELECT
    ) timeline()->addKey();

    scribbling = true;

    int imgX = width()/2 - animation()->animSize.width()/2;
    int imgY = height()/2 - animation()->animSize.height()/2;

    if (animation()->isKey(timeline()->getLayer(), timeline()->getPos())){
        if (currentTool == SELECT) selectTool->mousePress(event, imgX, imgY);
    }
    stroke << QPoint(event->pos().x() - imgX, event->pos().y() - imgY);

    update();
}

void Editor::mouseMoveEvent(QMouseEvent* event)
{
    if (!scribbling) return;

    int imgX = width()/2 - animation()->animSize.width()/2;
    int imgY = height()/2 - animation()->animSize.height()/2;

    switch (currentTool)
    {
        case PEN: break;
        case SELECT: selectTool->mouseMove(event); break;
        default: break;
    }

    stroke << QPoint(event->pos().x() - imgX, event->pos().y() - imgY);
//    updateCount += 1;
//    if (updateCount == updateRate) { update(); updateCount = 0; }
    update();
}

void Editor::mouseReleaseEvent(QMouseEvent* event)
{
    scribbling = false;

    int imgX = width()/2 - animation()->animSize.width()/2;
    int imgY = height()/2 - animation()->animSize.height()/2;

    switch (currentTool)
    {
//        case PEN: drawPenStroke(); break;
        case SHAPE: drawShape(); break;
        case FILL: drawFill(); break;
        case ERASER: if (animation()->isKey(timeline()->getLayer(), timeline()->getPos())) drawEraserStroke(); break;
        case SELECT: selectTool->mouseRelease(event, imgX, imgY); break;
        default: break;
    }
    stroke.clear();
    update();
}

void Editor::paintEvent(QPaintEvent*)
{
    globalPainter.begin(this);

    int imgX = width()/2 - animation()->animSize.width()/2;
    int imgY = height()/2 - animation()->animSize.height()/2;
    int imgH =animation()->animSize.height();
    int imgW = animation()->animSize.width();
    QPolygon s(stroke);
    s.translate(imgX, imgY);

    // Background
    globalPainter.drawImage(QPoint(0, 0), backgroundImage);

    globalPainter.setPen(editorBackgroundColor);
    globalPainter.setBrush(backgroundColor);
    globalPainter.drawRect(imgX, imgY, imgW, imgH);

    // Draw editor from layers
    animation()->foreachLayerRevert([this, imgX, imgY, &s](int i){
        if (animation()->getKeyCount(i) == 0) return;
        layerImage.fill(Qt::transparent);
        layerPainter.begin(&layerImage);

        // Draw current or previous keyframe
        if (animation()->isKey(i, getPos(i))) layerPainter.drawImage(QPoint(imgX, imgY), animation()->copyImageAt(i, getPos(i)));

        // Draw only on current layer
        if (i == timeline()->getLayer()){
            // Onionskin
            if (onionskinVisible)
            {
                QPainterPath path;
                path.addRect(0, 0, width(), height());
                int prev = animation()->getPrevKey(i, getPos());
                int prevprev = animation()->getPrevKey(i, prev);
                int next = animation()->getNextKey(i, getPos());
                int nextnext = animation()->getNextKey(i, next);

                if (onionskinloopVisible)
                {
                    if (getPos() == animation()->getFirstKey(i) && animation()->getKeyCount(i) > 3)
                        drawOnionSkin(&globalPainter, onionOpacityLoop, i, animation()->getLastKey(i), imgX, imgY, QColor(Qt::darkGreen));
                    if (getPos() == animation()->getLastKey(i) && animation()->getKeyCount(i) > 3)
                        drawOnionSkin(&globalPainter, onionOpacityLoop, i, animation()->getFirstKey(i), imgX, imgY, QColor(Qt::darkGreen));
                }

                if (prev < getPos() && prev != -1) drawOnionSkin(&globalPainter, onionOpacityFirst, i, prev, imgX, imgY, QColor(Qt::red));
                if (prevprev < getPos() && prevprev != -1 ) drawOnionSkin(&globalPainter, onionOpacitySecond, i, prevprev, imgX, imgY, QColor(Qt::red));
                if (next > getPos()) drawOnionSkin(&globalPainter, onionOpacityFirst, i, next, imgX, imgY, QColor(Qt::blue));
                if (nextnext > getPos()) drawOnionSkin(&globalPainter, onionOpacitySecond, i, nextnext, imgX, imgY, QColor(Qt::blue));

                globalPainter.setOpacity(1.0);
            }
            // Tool prview
            switch (currentTool)
            {
                case PEN: {
                    layerPainter.setPen(penTool);
                    if (s.count() == 1) layerPainter.drawPoint(s.first());
                    else if (s.count() > 1) layerPainter.drawPolyline(s);
                    break;
                } case SHAPE: {
                    if (s.count() < 2) break;
                    layerPainter.setPen(shapeTool);
                    switch(shapeSubtool){
                        case LINE: layerPainter.drawLine(s.first().x(), s.first().y(), s.last().x(), s.last().y()); break;
                        case RECTANGLE: layerPainter.drawRect(s.first().x(),s.first().y(),s.last().x() - s.first().x(),s.last().y() - s.first().y()); break;
                        case ELLIPSE: layerPainter.drawEllipse(s.first().x(), s.first().y(), s.last().x() - s.first().x(), s.last().y() - s.first().y()); break;
                        default: break;
                    }
                    break;
                } case FILL: {
                    if (s.count() < 2) break;
                    layerPainter.setPen(Qt::transparent);
                    layerPainter.setBrush(filltool);
                    switch(fillSubtool){
                        case LASSO: layerPainter.drawPolygon(s); break;
                        case RECTANGLE: layerPainter.drawRect(s.first().x(),s.first().y(),s.last().x() - s.first().x(),s.last().y() - s.first().y()); break;
                        case ELLIPSE: layerPainter.drawEllipse(s.first().x(),s.first().y(),s.last().x() - s.first().x(),s.last().y() - s.first().y()); break;
                        default: break;
                    }
                    break;
                } case ERASER: {
                    if (s.count() < 1) break;
                    QImage tempImg = layerImage.copy();
                    tempImg.fill(Qt::transparent);
                    QPainter p(&tempImg);
                    p.setPen(eraserTool);
                    if (s.count() == 1) p.drawPoint(s.first());
                    else if (s.count() > 1) p.drawPolyline(s);
                    layerPainter.setCompositionMode(QPainter::CompositionMode_DestinationOut);
                    layerPainter.drawImage(QPoint(0,0), tempImg);
                    layerPainter.setCompositionMode(QPainter::CompositionMode_SourceOver);
                    break;
                } case SELECT: {
                    selectTool->paintLayer(&layerPainter, imgX, imgY);
                    break;
                } default : break;
            }
        }
        globalPainter.setOpacity(timeline()->getLayerWidgetAt(i)->getLayerTitle()->getOpacity());
        globalPainter.drawImage(QPoint(0, 0), layerImage);
        globalPainter.setOpacity(1.0);

        layerPainter.end();
    });

    if (currentTool == ERASER && scribbling)
    {
        globalPainter.setPen(QPen(Qt::red, 2));
        globalPainter.setBrush(Qt::white);
        globalPainter.drawEllipse(s.last().x() -eraserTool.width()/2 , s.last().y() - eraserTool.width()/2, eraserTool.width(), eraserTool.width());
    }

    if (currentTool == SELECT)
    {
        selectTool->paintGlobal(&globalPainter);
    }

    globalPainter.end();
}

void Editor::drawPenStroke()
{
    QImage i = animation()->copyImageAt(timeline()->getLayer(), getPos());
    QImage j = i.copy();
    QPainter painter(&j);
    painter.setPen(penTool);
    if (stroke.count() == 1) painter.drawPoint(stroke.first());
    else if (stroke.count() > 1) painter.drawPolyline(stroke);
    undostack()->push(new ModifyImageCommand(i, j, timeline()->getLayer(), timeline()->getPos(), animation()));
}

void Editor::drawShape()
{
    QImage i = animation()->copyImageAt(timeline()->getLayer(), getPos());
    QImage j = i.copy();
    QPainter painter(&j);
    painter.setPen(shapeTool);
    switch(shapeSubtool){
        case LINE: painter.drawLine(stroke.first().x(), stroke.first().y(), stroke.last().x(), stroke.last().y()); break;
        case RECTANGLE: painter.drawRect(stroke.first().x(),stroke.first().y(),stroke.last().x() - stroke.first().x(),stroke.last().y() - stroke.first().y()); break;
        case ELLIPSE: painter.drawEllipse(stroke.first().x(),stroke.first().y(),stroke.last().x() - stroke.first().x(),stroke.last().y() - stroke.first().y()); break;
        default: break;
    }
    undostack()->push(new ModifyImageCommand(i, j, timeline()->getLayer(), timeline()->getPos(), animation()));
}

void Editor::drawFill()
{
    QImage i = animation()->copyImageAt(timeline()->getLayer(), getPos());
    QImage j = i.copy();
    QPainter painter(&j);
    painter.setPen(Qt::transparent);
    painter.setBrush(filltool);
    switch(fillSubtool){
        case LASSO: painter.drawPolygon(stroke); break;
        case RECTANGLE: painter.drawRect(stroke.first().x(),stroke.first().y(),stroke.last().x() - stroke.first().x(),stroke.last().y() - stroke.first().y()); break;
        case ELLIPSE: painter.drawEllipse(stroke.first().x(),stroke.first().y(),stroke.last().x() - stroke.first().x(),stroke.last().y() - stroke.first().y()); break;
        default: break;
    }
    undostack()->push(new ModifyImageCommand(i, j, timeline()->getLayer(), timeline()->getPos(), animation()));
}

void Editor::drawSelect()
{
    if (scribbling || !animation()->isKey(timeline()->getLayer(), timeline()->getPos())) return;

    int imgX = width()/2 - animation()->animSize.width()/2;
    int imgY = height()/2 - animation()->animSize.height()/2;

    selectTool->draw(imgX, imgY);
    selectTool->reset();
}

void Editor::drawEraserStroke()
{
    QImage i = animation()->copyImageAt(timeline()->getLayer(), timeline()->getPos());
    QImage j = i.copy();
    QImage k = i.copy();
    k.fill(Qt::transparent);
    QPainter painter(&k);
    painter.setPen(eraserTool);
    if (stroke.count() == 1) painter.drawPoint(stroke.first());
    else if (stroke.count() > 1) painter.drawPolyline(stroke);
    QPainter painter2(&j);
    painter2.setCompositionMode(QPainter::CompositionMode_DestinationOut);
    painter2.drawImage(QPoint(0,0), k);
    undostack()->push(new ModifyImageCommand(i, j, timeline()->getLayer(), timeline()->getPos(), animation()));
}

void Editor::knockback()
{
    if (scribbling || !animation()->isKey(timeline()->getLayer(), timeline()->getPos())) return;
    if (selectTool->state == STATE_SELECTED) {
        selectTool->knockback();
    } else {
        QImage i = animation()->copyImageAt(timeline()->getLayer(), timeline()->getPos());
        QImage j = i.copy();

        for (int y = 0; y < j.height(); y++) {
            QRgb* rgb = (QRgb*)j.scanLine(y);
            for (int x = 0; x < j.width(); x++) {
                rgb[x] = qRgba(qRed(rgb[x]), qGreen(rgb[x]), qBlue(rgb[x]), qAlpha(rgb[x]) > knockbackAmount ? qAlpha(rgb[x]) - knockbackAmount : 0 );
            }
        }
        undostack()->push(new ModifyImageCommand(i, j, timeline()->getLayer(), timeline()->getPos(), animation()));
    }
}

void Editor::clearImage()
{
    if (scribbling || !animation()->isKey(timeline()->getLayer(), timeline()->getPos())) return;
    if (selectTool->state == STATE_SELECTED) {
        selectTool->clear();
    } else {
        QImage i = animation()->copyImageAt(timeline()->getLayer(), timeline()->getPos());
        QImage j = i.copy();
        j.fill(Qt::transparent);
        undostack()->push(new ModifyImageCommand(i, j, timeline()->getLayer(), timeline()->getPos(), animation()));
    }
}

int Editor::getPos(int layer)
{
    if (layer == -1) layer = timeline()->getLayer();
    return animation()->isKey(layer, timeline()->getPos()) ?
    timeline()->getPos() :
    animation()->getPrevKey(layer, timeline()->getPos());
}

void Editor::drawOnionSkin(QPainter* painter, double opacity, int layer, int pos, int imgX, int imgY, QColor color)
{
    painter->setOpacity(opacity);
    QImage img = animation()->copyImageAt(layer, pos);
    QPainter p(&img);
    p.setCompositionMode(QPainter::CompositionMode_SourceAtop);
    p.setBrush(QBrush(color));
    p.setPen(QPen((color)));
    p.drawRect(0, 0, img.width(), img.height());
    painter->drawImage(QPoint(imgX, imgY), img);
}
